﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Logica.Turno;

namespace Logica
{
    public class Principal
    {
        
        List<Empleado> Empleados = new List<Empleado>();
        List<Turno> Turnos = new List<Turno>();
        List<Registro> Registros = new List<Registro>();
        List<Asistencia> Asistencias = new List<Asistencia>();

        public Principal()
        {
            CrearArchivo();
            Empleados = LeerArchivoEmpleado();
            Turnos = LeerArchivoTurno();

        }
        string rutaEmpleado = @"C:\PruebaProgTp2\ListaDeEmpleado.txt";
        string rutaTurno = @"C:\PruebaProgTp2\ListaDeTurno.txt";
        public void CrearArchivo()
        {
            if (!File.Exists(rutaEmpleado))
            {
                File.Create(rutaEmpleado).Close();
            }
            if (!File.Exists(rutaTurno))
            {
                File.Create(rutaTurno).Close();
            }
        }
        public int RegistrarLegajo()
        {
            int codigo = Empleados.Count() + 1;
            return codigo;
        }
        int numero = 0;
        public int ObtenerNumero()
        {
            numero += 1;
            return numero;
        }
        public List<Asistencia> CargarAsistencia(Empleado empleado, DateTime fechadeingreso, int legajo1, DateTime fechadesalida, DateTime fecha1, string dia1, string turno1, bool presente, int diastrabajados, int mediashorasextras, int tardanza, string texto)
        {
            Asistencia asistencia = new Asistencia();
            empleado.Asistencias.Add(asistencia);
            return Asistencias;
        }
        public Turno CargarTurnosEmpleado(Empleado empleado, string turno, int dia, DateTime horadellegada, DateTime horadesalida, int estado2)
        {

            Turno nuevo2 = new Turno();
            nuevo2.Numero = ObtenerNumero();
            nuevo2.TurnoTrabajo = turno;
            nuevo2.Dia = DevolverDia(nuevo2, dia);
            nuevo2.HoraDesde = Convert.ToDateTime(horadellegada);
            nuevo2.HoraHasta = Convert.ToDateTime(horadesalida);
            nuevo2.Estado = estado2;
            empleado.Turno = nuevo2;
            GuardarTurno();
            return nuevo2;
        }
        public void CargarTurnos(string turno, int dia, DateTime horadellegada, DateTime horadesalida, int estado2)
        {
            Turno nuevo2 = new Turno();
            nuevo2.Numero = ObtenerNumero();
            nuevo2.TurnoTrabajo = turno;
            nuevo2.Dia = DevolverDia(nuevo2, dia);
            nuevo2.HoraDesde = Convert.ToDateTime(horadellegada);
            nuevo2.HoraHasta = Convert.ToDateTime(horadesalida);
            nuevo2.Estado = estado2;
            GuardarTurno();
        }
        public TIPO DevolverDia(Turno nuevo2, int dia)
        {
            switch (dia)
            {
                case 0:
                    nuevo2.Dia = TIPO.Domingo;
                    break;
                case 1:
                    nuevo2.Dia = TIPO.Lunes;
                    break;
                case 2:
                    nuevo2.Dia = TIPO.Martes;
                    break;
                case 3:
                    nuevo2.Dia = TIPO.Miercoles;
                    break;
                case 4:
                    nuevo2.Dia = TIPO.Jueves;
                    break;
                case 5:
                    nuevo2.Dia = TIPO.Viernes;
                    break;
                default:
                    nuevo2.Dia = TIPO.Sabado;
                    break;
            }
            return nuevo2.Dia;
        }
        public void CargarEmpleado(string nombre, string domicilio, string localidad, decimal sueldo, int telefono, string correo, int estado, string turno, int dia, DateTime horadellegada, DateTime horadesalida, int estado2)
        {
            Empleado nuevo = new Empleado();
            nuevo.Legajo = RegistrarLegajo();
            nuevo.NombreYApellido = nombre;
            nuevo.Domicilio = domicilio;
            nuevo.Localidad = localidad;
            nuevo.Telefono = telefono;
            nuevo.CorreoElectronico = correo;
            nuevo.SueldoNeto = sueldo;
            nuevo.Turno = CargarTurnosEmpleado(nuevo, turno, dia, horadellegada, horadesalida, estado2);
            nuevo.Estado = estado;
            Empleados.Add(nuevo);
            GuardarEmpleado();
        }
        
        public List<Turno> LeerArchivoTurno()
        {
            string Archivo = rutaTurno;
            using (StreamReader Lector = new StreamReader(Archivo))
            {
                List<Turno> Turno = new List<Turno>();
                string contenido = Lector.ReadToEnd();
                if (contenido != "")
                {
                    Turno = JsonConvert.DeserializeObject<List<Turno>>(contenido);
                }
                return Turno;
            }
        }
        public bool GuardarTurno()
        {
            string Archivo = rutaTurno;
            using (StreamWriter Escritura = new StreamWriter(Archivo, false))
            {
                string contenido = JsonConvert.SerializeObject(Turnos);
                Escritura.Write(contenido);
                return true;
            }
        }
        public List<Empleado> LeerArchivoEmpleado()
        {
            string Archivo = rutaEmpleado;
            using (StreamReader Lector = new StreamReader(Archivo))
            {
                List<Empleado> Empleados = new List<Empleado>();
                string contenido = Lector.ReadToEnd();
                if (contenido != "")
                {
                    Empleados = JsonConvert.DeserializeObject<List<Empleado>>(contenido);
                }
                return Empleados;
            }
        }
        public bool GuardarEmpleado()
        {
            string Archivo = rutaEmpleado;
            using (StreamWriter Escritura = new StreamWriter(Archivo, false))
            {
                string contenido = JsonConvert.SerializeObject(Empleados);
                Escritura.Write(contenido);
                return true;
            }
        }
        public List<Asistencia> LeerArchivoAsistencia()
        {
            string Archivo = rutaEmpleado;
            using (StreamReader Lector = new StreamReader(Archivo))
            {
                List<Asistencia> Asistencias = new List<Asistencia>();
                string contenido = Lector.ReadToEnd();
                if (contenido != "")
                {
                    Asistencias = JsonConvert.DeserializeObject<List<Asistencia>>(contenido);
                }
                return Asistencias;
            }
        }
        public bool GuardarAsistencia()
        {
            string Archivo = rutaEmpleado;
            using (StreamWriter Escritura = new StreamWriter(Archivo, false))
            {
                string contenido = JsonConvert.SerializeObject(Asistencias);
                Escritura.Write(contenido);
                return true;
            }
        }
        public Empleado BuscarEmpleadoPorLegajo(int? legajo)
        {
            foreach (var item in Empleados)
            {
                if (item.Legajo == legajo)
                {
                    return item;
                }
            }
            return null;
        }
        public List<Empleado> BuscarParcial(string letra)
        {
            if (letra == string.Empty)
            {
                return Empleados;
            }
            List<Empleado> empleado = new List<Empleado>();
            foreach (var item in Empleados)
            {
                if (item.NombreYApellido.Contains(letra))
                {
                    empleado.Add(item);
                }
            }
            return empleado;
        }
        public void ModificarEmpleado(int legajo, string nombre, string domicilio, string correo, string localidad, int telefono, int estado, decimal sueldoneto)
        {
            BuscarEmpleadoPorLegajo(legajo);
            foreach (var item in Empleados)
            {
                if (item.Legajo == legajo)
                {
                    if (item.NombreYApellido != nombre)
                    {
                        item.NombreYApellido = nombre;
                    }
                    if (item.Domicilio != domicilio)
                    {
                        item.Domicilio = domicilio;
                    }
                    if (item.CorreoElectronico != correo)
                    {
                        item.CorreoElectronico = correo;
                    }
                    if (item.Localidad != localidad)
                    {
                        item.Localidad = localidad;
                    }
                    if (item.Telefono != telefono)
                    {
                        item.Telefono = telefono;
                    }
                    if (item.Estado != estado)
                    {
                        item.Estado = estado;
                    }
                    if (item.SueldoNeto != sueldoneto)
                    {
                        item.SueldoNeto = sueldoneto;
                    }
                }
                GuardarEmpleado();
            }
        }
        public void EliminarEmpleado(int legajo)
        {
            foreach (Empleado item in Empleados)
            {
                if (item.Legajo == legajo)
                {
                    item.Estado = 99;
                    GuardarEmpleado();
                    break;
                }
            }
        }
        public void ModificarTurno(Empleado empleado, int numero)
        {
            foreach (var item in Empleados)
            {
                if (item.Legajo == empleado.Legajo)
                {
                    switch (numero)
                    {
                        case 1:
                            item.Turno.Estado = empleado.Turno.Estado;
                            break;
                        case 2:
                            item.Turno.Dia = empleado.Turno.Dia;
                            break;
                        case 3:
                            item.Turno.HoraDesde = empleado.Turno.HoraDesde;
                            break;
                        case 4:
                            item.Turno.HoraHasta = empleado.Turno.HoraHasta;
                            break;
                        default:
                            item.Turno.Numero = empleado.Turno.Numero;
                            break;
                    }
                }
            }
        }
        public Turno BuscarTurnoXDescripcion(string descripcion)
        {
            foreach (var item in Turnos)
            {
                if (item.TurnoTrabajo == descripcion)
                {
                    return item;
                }
            }
            return null;
        }
        public Turno BuscarTurnoXFecha(int fecha)
        {
            foreach (var item in Turnos)
            {
                if ((int)item.Dia == fecha)
                {
                    return item;
                }
            }
            return null;
        }
        public void EliminarTurno(string descripcion)
        {
            foreach (var item in Turnos)
            {
                if (item.TurnoTrabajo == descripcion)
                {
                    if (item.Estado == 0)
                    {
                        Turnos.Remove(item);
                        GuardarTurno();
                        break;
                    }
                }
            }
        }
        public List<Asistencia> RegistrarEntrada(DateTime ingreso, int legajo, DateTime salida, DateTime fecha, string dia, string turno)
        {
            List<Asistencia> NuevaAsistencia = new List<Asistencia>();
            int tardanza = 0, diastrabajados = 0;

            foreach (var item in Empleados)
            {
                if (item.Legajo == legajo)
                {
                    Asistencia asist = new Asistencia();
                    if ((int)item.Turno.Dia == 6 || item.Turno.Dia == 0)
                    {
                        asist.Texto = "El ingreso no es válido por el dia.";
                    }
                    else
                    {
                        asist.Dia = Convert.ToString(item.Turno.Dia);
                        asist.Legajo = legajo;
                        asist.FechaDeIngreso = item.Turno.HoraDesde;
                        if (ingreso.Minute > item.Turno.HoraDesde.Minute)
                        {
                            double resultado = (item.Turno.HoraDesde - ingreso).TotalMinutes;
                            asist.MediasHorasExtras -= (int)(resultado / 30);
                            asist.Texto = $"Llego {asist.MediasHorasExtras} medias horas tarde";
                        }
                        else
                        {
                            double resultado = (item.Turno.HoraDesde - ingreso).TotalMinutes;
                            asist.MediasHorasExtras += (int)(resultado / 30);
                            asist.Texto = $"Llego {asist.MediasHorasExtras} medias horas tamrano";
                        }
                        asist.FechaDeSalida = item.Turno.HoraHasta;
                        if (salida.Minute > item.Turno.HoraHasta.Minute)
                        {
                            double resultado = (item.Turno.HoraHasta - salida).TotalMinutes;
                            asist.MediasHorasExtras += (int)(resultado / 30);
                            asist.Texto = $"Se quedo {asist.MediasHorasExtras} medias horas mas";

                        }
                        else
                        {
                            double resultado = (item.Turno.HoraHasta - salida).TotalMinutes;
                            asist.MediasHorasExtras -= (int)(resultado / 30);
                            asist.Texto = $"Salio {asist.MediasHorasExtras} medias horas antes";

                        }
                        if (ingreso > item.Turno.HoraDesde.AddMinutes(15))
                        {
                            tardanza += 1;
                        }
                        if (ingreso == null)
                        {
                            asist.Presente = false;
                            asist.Texto = ("El empleado no vino a trabajar");
                        }
                        diastrabajados++;
                        asist.DiasTrabajados = diastrabajados;
                        asist.Tardanzas = tardanza;
                        asist.Fecha = fecha;
                        asist.Dia = dia;
                        asist.Turno = turno;
                    }
                    NuevaAsistencia.Add(asist);
                }               
            }
            return Asistencias;
        }
        public List<Empleado> ObtenerEmpleados(int? legajo)
        {
           return Empleados.Where(x => legajo.HasValue ? x.Legajo == legajo : true).ToList();           
        }
        public List<Turno> ObtenerTurnos(int? numero)
        {
            return Turnos.Where(x => numero.HasValue ? x.Numero == numero : true).ToList();
        }
        public List<Asistencia> ObtenerRegistro(int? legajo)
        {
            return Asistencias.Where(x => legajo.HasValue ? x.Legajo == legajo : true).ToList();
        }
        public void GuardarRegistro(int legajo, string dia, DateTime ingreso, DateTime salida, DateTime fecha, string turno)
        {
            Asistencia informe = new Asistencia();
            informe.Legajo = legajo;
            informe.Dia = dia;
            informe.FechaDeIngreso = Convert.ToDateTime(ingreso);
            informe.FechaDeSalida = Convert.ToDateTime(salida);
            informe.Fecha = Convert.ToDateTime(fecha);
            informe.Turno = turno;
            Asistencias.Add(informe);
            
        }    
    }
    
}
