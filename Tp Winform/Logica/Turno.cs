﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Turno
    {
        public int Numero { get; set; }
        public string TurnoTrabajo { get; set; }
        public enum TIPO { Domingo = 0, Lunes = 1, Martes = 2, Miercoles = 3, Jueves = 4, Viernes = 5, Sabado = 6 }
        public TIPO Dia { get; set; }
        public DateTime HoraDesde { get; set; }
        public DateTime HoraHasta { get; set; }
        public int Estado { get; set; }
    }
}
