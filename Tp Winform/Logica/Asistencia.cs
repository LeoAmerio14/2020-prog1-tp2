﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Asistencia
    {
        public string Dia { get; set; }
        public DateTime FechaDeIngreso { get; set; }
        public DateTime FechaDeSalida { get; set; }
        public int Legajo { get; set; }
        public bool Presente { get; set; }
        public int DiasTrabajados { get; set; }
        public int MediasHorasExtras { get; set; }
        public int Tardanzas { get; set; }
        public DateTime Fecha { get; set; }
        public string Texto { get; set; }
        public string Turno { get; set; }
        /*public Asistencia(string dia1, DateTime fechadeingreso, DateTime fechadesalida, int legajo1, bool presente, int diastrabajados, int mediashorasextras, int tardanza, DateTime fecha1, string texto, string turno1)
        {
            if (presente)
            {
                Dia = dia1;
                FechaDeIngreso = fechadeingreso;
                FechaDeSalida = fechadesalida;
                Legajo = legajo1;
                DiasTrabajados = diastrabajados;
                MediasHorasExtras = mediashorasextras;
                Tardanzas = tardanza;
                Fecha = fecha1;
                Texto = texto;
                Turno = turno1;
            }
        }*/
    }
}
