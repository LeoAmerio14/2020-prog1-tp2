﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Empleado
    {
        /*public Empleado(string nombre, string domicilio, string localidad, decimal sueldo)
        {
            NombreYApellido = nombre;
            Domicilio = domicilio;
            Localidad = localidad;
            SueldoNeto = sueldo;
        }*/

        public int Legajo { get; set; }
        public string NombreYApellido { get; set; }
        public string Domicilio { get; set; }
        public string Localidad { get; set; }
        public int Telefono { get; set; }
        public string CorreoElectronico { get; set; }
        public decimal SueldoNeto { get; set; }
        public Turno Turno { get; set; }
        public int Estado { get; set; }
        public List<Asistencia> Asistencias { get; set; }
    }
}
