﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Registro
    {
        public string Dia { get; set; }
        public DateTime Ingreso { get; set; }
        public DateTime Salida { get; set; }
        public DateTime Fecha { get; set; }
        public int Turno { get; set; }

        public Registro(Empleado empleado)
        {
            Fecha = DateTime.Today;
            if (empleado != null)
            {
                Turno = empleado.Turno.Numero;
            }
            Ingreso = DateTime.Now;
        }

        /*internal static object Where(Func<object, bool> p)
        {
            throw new NotImplementedException();
        }*/
    }
}
