﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logica;

namespace WindowsForms_Tp_Prog
{
    interface IModificarEmpleado
    {
        void ModificarEmpleado(int v1, string text1, string text2, string text3, string text4, int v2, int v3, decimal v4);
        void GuardarEmpleado();
    }
}
