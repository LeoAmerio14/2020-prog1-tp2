﻿namespace WindowsForms_Tp_Prog
{
    partial class FormModificarEmpleado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtnuevoEstado = new System.Windows.Forms.TextBox();
            this.txtnuevoSueldo = new System.Windows.Forms.TextBox();
            this.txtnuevoCorreo = new System.Windows.Forms.TextBox();
            this.txtnuevoTelefono = new System.Windows.Forms.TextBox();
            this.txtnuevoLocalidad = new System.Windows.Forms.TextBox();
            this.txtnuevoDomicilio = new System.Windows.Forms.TextBox();
            this.txtnuevoNombre = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLegajo = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 41);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(126, 17);
            this.label8.TabIndex = 44;
            this.label8.Text = "Nombre y apellido:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(71, 94);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 17);
            this.label7.TabIndex = 43;
            this.label7.Text = "Domicilio:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(296, 155);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 17);
            this.label6.TabIndex = 42;
            this.label6.Text = "Localidad:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(71, 155);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 17);
            this.label5.TabIndex = 41;
            this.label5.Text = "Telefono:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(305, 214);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 17);
            this.label4.TabIndex = 40;
            this.label4.Text = "Correo:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(83, 214);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 17);
            this.label3.TabIndex = 39;
            this.label3.Text = "Sueldo:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(313, 94);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 37;
            this.label1.Text = "Estado:";
            // 
            // txtnuevoEstado
            // 
            this.txtnuevoEstado.Location = new System.Drawing.Point(377, 91);
            this.txtnuevoEstado.Margin = new System.Windows.Forms.Padding(4);
            this.txtnuevoEstado.Multiline = true;
            this.txtnuevoEstado.Name = "txtnuevoEstado";
            this.txtnuevoEstado.Size = new System.Drawing.Size(108, 34);
            this.txtnuevoEstado.TabIndex = 36;
            // 
            // txtnuevoSueldo
            // 
            this.txtnuevoSueldo.Location = new System.Drawing.Point(156, 211);
            this.txtnuevoSueldo.Margin = new System.Windows.Forms.Padding(4);
            this.txtnuevoSueldo.Multiline = true;
            this.txtnuevoSueldo.Name = "txtnuevoSueldo";
            this.txtnuevoSueldo.Size = new System.Drawing.Size(108, 34);
            this.txtnuevoSueldo.TabIndex = 34;
            // 
            // txtnuevoCorreo
            // 
            this.txtnuevoCorreo.Location = new System.Drawing.Point(377, 211);
            this.txtnuevoCorreo.Margin = new System.Windows.Forms.Padding(4);
            this.txtnuevoCorreo.Multiline = true;
            this.txtnuevoCorreo.Name = "txtnuevoCorreo";
            this.txtnuevoCorreo.Size = new System.Drawing.Size(108, 34);
            this.txtnuevoCorreo.TabIndex = 33;
            // 
            // txtnuevoTelefono
            // 
            this.txtnuevoTelefono.Location = new System.Drawing.Point(156, 152);
            this.txtnuevoTelefono.Margin = new System.Windows.Forms.Padding(4);
            this.txtnuevoTelefono.Multiline = true;
            this.txtnuevoTelefono.Name = "txtnuevoTelefono";
            this.txtnuevoTelefono.Size = new System.Drawing.Size(108, 34);
            this.txtnuevoTelefono.TabIndex = 32;
            // 
            // txtnuevoLocalidad
            // 
            this.txtnuevoLocalidad.Location = new System.Drawing.Point(377, 152);
            this.txtnuevoLocalidad.Margin = new System.Windows.Forms.Padding(4);
            this.txtnuevoLocalidad.Multiline = true;
            this.txtnuevoLocalidad.Name = "txtnuevoLocalidad";
            this.txtnuevoLocalidad.Size = new System.Drawing.Size(108, 34);
            this.txtnuevoLocalidad.TabIndex = 31;
            // 
            // txtnuevoDomicilio
            // 
            this.txtnuevoDomicilio.Location = new System.Drawing.Point(156, 91);
            this.txtnuevoDomicilio.Margin = new System.Windows.Forms.Padding(4);
            this.txtnuevoDomicilio.Multiline = true;
            this.txtnuevoDomicilio.Name = "txtnuevoDomicilio";
            this.txtnuevoDomicilio.Size = new System.Drawing.Size(108, 34);
            this.txtnuevoDomicilio.TabIndex = 30;
            // 
            // txtnuevoNombre
            // 
            this.txtnuevoNombre.Location = new System.Drawing.Point(156, 38);
            this.txtnuevoNombre.Margin = new System.Windows.Forms.Padding(4);
            this.txtnuevoNombre.Multiline = true;
            this.txtnuevoNombre.Name = "txtnuevoNombre";
            this.txtnuevoNombre.Size = new System.Drawing.Size(108, 34);
            this.txtnuevoNombre.TabIndex = 29;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(211, 303);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(185, 58);
            this.button1.TabIndex = 28;
            this.button1.Text = "Agregar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(305, 41);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 17);
            this.label2.TabIndex = 46;
            this.label2.Text = "Legajo:";
            // 
            // txtLegajo
            // 
            this.txtLegajo.Location = new System.Drawing.Point(377, 38);
            this.txtLegajo.Margin = new System.Windows.Forms.Padding(4);
            this.txtLegajo.Multiline = true;
            this.txtLegajo.Name = "txtLegajo";
            this.txtLegajo.Size = new System.Drawing.Size(108, 34);
            this.txtLegajo.TabIndex = 45;
            // 
            // FormModificarEmpleado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(569, 380);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtLegajo);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtnuevoEstado);
            this.Controls.Add(this.txtnuevoSueldo);
            this.Controls.Add(this.txtnuevoCorreo);
            this.Controls.Add(this.txtnuevoTelefono);
            this.Controls.Add(this.txtnuevoLocalidad);
            this.Controls.Add(this.txtnuevoDomicilio);
            this.Controls.Add(this.txtnuevoNombre);
            this.Controls.Add(this.button1);
            this.Name = "FormModificarEmpleado";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.FormModificarEmpleado_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtnuevoEstado;
        private System.Windows.Forms.TextBox txtnuevoSueldo;
        private System.Windows.Forms.TextBox txtnuevoCorreo;
        private System.Windows.Forms.TextBox txtnuevoTelefono;
        private System.Windows.Forms.TextBox txtnuevoLocalidad;
        private System.Windows.Forms.TextBox txtnuevoDomicilio;
        private System.Windows.Forms.TextBox txtnuevoNombre;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtLegajo;
    }
}