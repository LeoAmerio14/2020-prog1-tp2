﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logica;

namespace WindowsForms_Tp_Prog
{
    interface IAgregarTurno
    {
        void GuardarTurno();
        void CargarTurnos(string turno, int dia, DateTime horadellegada, DateTime horadesalida, int estado2);
        Turno CargarTurnosEmpleado(Empleado empleado, string turno, int dia, DateTime horadellegada, DateTime horadesalida, int estado2);
    }
}
