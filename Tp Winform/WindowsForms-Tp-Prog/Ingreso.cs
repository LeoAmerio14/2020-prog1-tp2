﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logica;

namespace WindowsForms_Tp_Prog
{
    public partial class Ingreso : Form, IIngreso
    {     
        public Ingreso()
        {
            InitializeComponent();
        }
        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            IPrincipal principal = this.Owner as IPrincipal;
            principal.RegistrarEntrada(Convert.ToDateTime(txtRIngreso.Text), Convert.ToInt32(txtRLegajo.Text), Convert.ToDateTime(txtRSalida.Text), Convert.ToDateTime(txtRFecha.Text), Convert.ToString(txtDia.Text), txtRTurno.Text);
            principal.GuardarRegistro(Convert.ToInt32(txtRLegajo.Text), Convert.ToString(txtDia.Text), Convert.ToDateTime(txtRIngreso.Text), Convert.ToDateTime(txtRSalida.Text), Convert.ToDateTime(txtRFecha.Text), txtRTurno.Text);
            
            MessageBox.Show("Se registro correctamente.");
          
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            IPrincipal principal = this.Owner as IPrincipal;
            List<Asistencia> registro = principal.ObtenerRegistro(null);

            dataGridView1.DataSource = registro;
            dataGridView1.AutoGenerateColumns = false;
        }

        private void Ingreso_Load(object sender, EventArgs e)
        {
            IPrincipal principal = this.Owner as IPrincipal;
            List<Asistencia> registro = principal.ObtenerRegistro(null);

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = registro;
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            IPrincipal principal = this.Owner as IPrincipal;
            List<Asistencia> registros = principal.ObtenerRegistro(null);

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = registros;
            
        }

        public List<Asistencia> RegistrarEntrada(DateTime ingreso, int legajo, DateTime salida, DateTime fecha, string dia, string turno)
        {
            IPrincipal principal = this.Owner as IPrincipal;
            List<Asistencia> asistencias = new List<Asistencia>();
            return asistencias = principal.RegistrarEntrada(ingreso, legajo, salida, fecha, dia, turno);
        }

        public void GuardarRegistro(int legajo, string dia, DateTime ingreso, DateTime salida, DateTime fecha, string turno)
        {
            IPrincipal principal = this.Owner as IPrincipal;
            principal.GuardarRegistro(legajo, dia, ingreso, salida, fecha, turno);
        }

        public List<Asistencia> ObtenerRegistro(int legajo)
        {
            IPrincipal principal = this.Owner as IPrincipal;
            List<Asistencia> registros = new List<Asistencia>();
            return registros = principal.ObtenerRegistro(legajo);
        }
    }
}
