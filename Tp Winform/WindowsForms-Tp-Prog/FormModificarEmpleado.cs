﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logica;

namespace WindowsForms_Tp_Prog
{
    public partial class FormModificarEmpleado : Form, IModificarEmpleado
    {
        public FormModificarEmpleado(FormBuscarEmpleado.Datos info)
        {
            InitializeComponent();
            txtLegajo.Text = Convert.ToString(info.legajo);
            txtnuevoNombre.Text = info.Nombre;
            txtnuevoDomicilio.Text = info.Domicilio;
            txtnuevoLocalidad.Text = info.Localidad;
            txtnuevoCorreo.Text = info.Correo; 
            txtnuevoTelefono.Text = Convert.ToString(info.telefono);
            txtnuevoSueldo.Text = Convert.ToString(info.sueldo);
            txtnuevoEstado.Text = Convert.ToString(info.estado);
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            IPrincipal servicio = this.Owner as IPrincipal;
            servicio.ModificarEmpleado(Convert.ToInt32(txtLegajo.Text), txtnuevoNombre.Text, txtnuevoDomicilio.Text, txtnuevoCorreo.Text, txtnuevoLocalidad.Text, Convert.ToInt32(txtnuevoTelefono.Text), Convert.ToInt32(txtnuevoEstado.Text), Convert.ToDecimal(txtnuevoSueldo.Text));
            servicio.GuardarEmpleado();
            MessageBox.Show("Se modifico correctamente.");
        }

        private void FormModificarEmpleado_Load(object sender, EventArgs e)
        {

        }
        public void GuardarEmpleado()
        {
            IPrincipal servicio = this.Owner as IPrincipal;
            servicio.GuardarEmpleado();
        }

        public void ModificarEmpleado(int legajo, string nombre, string domicilio, string correo, string localidad, int telefono, int estado, decimal sueldoneto)
        {
            IPrincipal servicio = this.Owner as IPrincipal;
            servicio.ModificarEmpleado(legajo, nombre, domicilio, correo, localidad, telefono, estado, sueldoneto);
        }
    }
}
