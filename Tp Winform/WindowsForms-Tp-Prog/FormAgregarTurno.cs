﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logica;

namespace WindowsForms_Tp_Prog
{

    public partial class FormAgregarTurno : Form, IAgregarTurno
    {
        public FormAgregarTurno()
        {
            InitializeComponent();
        }

        public void CargarTurnos(string turno, int dia, DateTime horadellegada, DateTime horadesalida, int estado2)
        {
            IPrincipal principal = this.Owner as IPrincipal;
            principal.CargarTurnos(turno, dia, horadellegada, horadesalida, estado2);
        }

        public Turno CargarTurnosEmpleado(Empleado empleado, string turno, int dia, DateTime horadellegada, DateTime horadesalida, int estado2)
        {
            IPrincipal principal = this.Owner as IPrincipal;
            Turno turno1 = new Turno();
            return turno1 = principal.CargarTurnosEmpleado(empleado, turno, dia, horadellegada, horadesalida, estado2);
        }

        public void GuardarTurno()
        {
            IPrincipal principal = this.Owner as IPrincipal;
            principal.GuardarTurno();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            IPrincipal principal = this.Owner as IPrincipal;
            principal.CargarTurnos(txtTurno.Text, Convert.ToInt32(txtDia.Text), Convert.ToDateTime(txtEntrada.Text), Convert.ToDateTime(txtSalida.Text), Convert.ToInt32(txtEstado.Text));
            principal.GuardarTurno();
            MessageBox.Show("Se cargaron los datos correctamente.");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form nuevo = new PantallaPrincipal();
            nuevo.Show();
        }

        private void FormAgregarTurno_Load(object sender, EventArgs e)
        {

        }
    }
}
