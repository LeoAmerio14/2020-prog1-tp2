﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logica;

namespace WindowsForms_Tp_Prog
{
    public partial class FormAgregarEmpleado : Form, IAgregarEmpleado
    {
        //private IAgregarEmpleado agregarEmpleado;
        public FormAgregarEmpleado()
        {
            InitializeComponent();
        }

        public void CargarEmpleado(string nombre, string domicilio, string localidad, decimal sueldo, int telefono, string correo, int estado, string turno, int dia, DateTime horadellegada, DateTime horadesalida, int estado2)
        {
            IPrincipal principal = this.Owner as IPrincipal;
            principal.CargarEmpleado(nombre, domicilio, localidad, sueldo, telefono, correo, estado, turno, dia, horadellegada, horadesalida, estado2);
        }

        public void CargarTurnos(string text, int v1, DateTime dateTime1, DateTime dateTime2, int v2)
        {
            IPrincipal principal = this.Owner as IPrincipal;
            principal.CargarTurnos(text, v1, dateTime1, dateTime2, v2);
        }

        private void BtnAgregarEmpelado_Click(object sender, EventArgs e)
        {
            IPrincipal principal = this.Owner as IPrincipal;
            if (Convert.ToInt32(txtDia.Text) == 0 || Convert.ToInt32(txtDia.Text) == 6)
            {
                MessageBox.Show("Dia Ingresado no valido.");
            }
            else
            {
                principal.CargarEmpleado(txtNombre.Text, txtDomicilio.Text, txtLocalidad.Text,Convert.ToDecimal(txtSueldo.Text) ,Convert.ToInt32(txtTelefono.Text), txtCorreo.Text, Convert.ToInt32(txtEstado.Text), txtTurno.Text, Convert.ToInt32(txtDia.Text), Convert.ToDateTime(txtEntrada.Text), Convert.ToDateTime(txtSalida.Text), Convert.ToInt32(txtEstado2.Text));
                principal.CargarTurnos(txtTurno.Text, Convert.ToInt32(txtDia.Text), Convert.ToDateTime(txtEntrada.Text), Convert.ToDateTime(txtSalida.Text), Convert.ToInt32(txtEstado2.Text)); 
                
                MessageBox.Show("Se agrego correctamente.");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form nuevo = new PantallaPrincipal();
            nuevo.Show();
        }

        private void FormAgregarEmpleado_Load(object sender, EventArgs e)
        {

        }
    }
}
