﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logica;

namespace WindowsForms_Tp_Prog
{
    interface IAgregarEmpleado
    {
        void CargarEmpleado(string nombre, string domicilio, string localidad, decimal sueldo, int telefono, string correo, int estado, string turno, int dia, DateTime horadellegada, DateTime horadesalida, int estado2);
        void CargarTurnos(string text, int v1, DateTime dateTime1, DateTime dateTime2, int v2);
    }
}
