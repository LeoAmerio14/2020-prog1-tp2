﻿using Logica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsForms_Tp_Prog
{
    interface IIngreso
    {
        List<Asistencia> RegistrarEntrada(DateTime ingreso, int legajo, DateTime salida, DateTime fecha, string dia, string turno);
        void GuardarRegistro(int legajo, string dia, DateTime ingreso, DateTime salida, DateTime fecha, string turno);
        List<Asistencia> ObtenerRegistro(int legajo);
    }
}
