﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logica;

namespace WindowsForms_Tp_Prog
{

    public partial class FormBuscarEmpleado : Form, IBuscarEmpleado
    {
        //private IBuscarEmpleado servicio;
        public FormBuscarEmpleado()
        {
            InitializeComponent();
        }
        
        private void btnBuscar_Click(object sender, EventArgs e)
        {
            IPrincipal servicio = this.Owner as IPrincipal;
            Empleado empleados = servicio.BuscarEmpleadoPorLegajo(Convert.ToInt32(txtLegajo.Text));
            var g = new List<Empleado>();
            g.Add(empleados);
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = g;

        }
        private void btnBuscarNombre_Click(object sender, EventArgs e)
        {
            IPrincipal servicio = this.Owner as IPrincipal;
            var resultado = servicio.BuscarParcial(txtNombre.Text);
            dataGridView1.DataSource = resultado;
        }
        private void btnActualizacion_Click(object sender, EventArgs e)
        {
            IPrincipal servicio = this.Owner as IPrincipal;
            List<Empleado> empleados = servicio.ObtenerEmpleados(null);

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = empleados;
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            IPrincipal servicio = this.Owner as IPrincipal;
            List<Empleado> empleados = servicio.ObtenerEmpleados(null);

            dataGridView1.DataSource = empleados;
            dataGridView1.AutoGenerateColumns = false;

        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            IPrincipal servicio = this.Owner as IPrincipal;
            servicio.EliminarEmpleado(Convert.ToInt32(txtLegajo.Text));
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Form nuevo = new PantallaPrincipal();
            nuevo.Show();
        }
        public struct Datos
        {
            public int legajo;
            public string Nombre;
            public string Domicilio;
            public string Localidad;
            public int telefono;
            public string Correo;
            public decimal sueldo;
            public int estado;
        }
        private void BtnModificar_Click(object sender, EventArgs e)
        {
            IPrincipal servicio = this.Owner as IPrincipal;
            Datos info;
            var emp = servicio.BuscarEmpleadoPorLegajo(Convert.ToInt32(txtLegajo.Text));
            info.legajo = emp.Legajo;
            info.Nombre = emp.NombreYApellido;
            info.Domicilio = emp.Domicilio;
            info.Localidad = emp.Localidad;
            info.telefono = emp.Telefono;
            info.Correo = emp.CorreoElectronico;
            info.sueldo = emp.SueldoNeto;
            info.estado = emp.Estado;

            Form nuevo = new FormModificarEmpleado(info);
            nuevo.Show();
            this.Close();
        }
        private void FormBuscarEmpleado_Load(object sender, EventArgs e)
        {
            IPrincipal servicio = this.Owner as IPrincipal;
            List<Empleado> empleados = servicio.ObtenerEmpleados(null);

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = empleados;
        }

        public Empleado BuscarEmpleadoPorLegajo(int legajo)
        {
            Empleado empleado = new Empleado();
            IPrincipal principal = this.Owner as IPrincipal;
            empleado = principal.BuscarEmpleadoPorLegajo(legajo);
            return empleado;
        }

        public void EliminarEmpleado(int legajo)
        {
            IPrincipal principal = this.Owner as IPrincipal;
            principal.EliminarEmpleado(legajo);
        }

        public List<Empleado> ObtenerEmpleados(int? legajo)
        {
            List<Empleado> empleados = new List<Empleado>();
            IPrincipal principal = this.Owner as IPrincipal;
            empleados = principal.ObtenerEmpleados(legajo);
            return empleados;
        }

        object IBuscarEmpleado.BuscarParcial(string text)
        {
            List<Empleado> empleados = new List<Empleado>();
            IPrincipal principal = this.Owner as IPrincipal;
            empleados = (List<Empleado>)principal.BuscarParcial(text);
            return empleados;
        }
    }
}
