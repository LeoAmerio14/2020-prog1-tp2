﻿namespace WindowsForms_Tp_Prog
{
    partial class FormBuscarTurno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnModificarTurno = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnBuscarDiaSemana = new System.Windows.Forms.Button();
            this.btnBuscarDescripcion = new System.Windows.Forms.Button();
            this.btnActualizacion = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.txtDiaSemana = new System.Windows.Forms.TextBox();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnEliminarT = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnModificarTurno
            // 
            this.BtnModificarTurno.Location = new System.Drawing.Point(457, 369);
            this.BtnModificarTurno.Margin = new System.Windows.Forms.Padding(4);
            this.BtnModificarTurno.Name = "BtnModificarTurno";
            this.BtnModificarTurno.Size = new System.Drawing.Size(138, 52);
            this.BtnModificarTurno.TabIndex = 61;
            this.BtnModificarTurno.Text = "Modificar ";
            this.BtnModificarTurno.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(231, 370);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(208, 52);
            this.button2.TabIndex = 60;
            this.button2.Text = "Volver a la pantalla principal";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // btnBuscarDiaSemana
            // 
            this.btnBuscarDiaSemana.Location = new System.Drawing.Point(651, 24);
            this.btnBuscarDiaSemana.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnBuscarDiaSemana.Name = "btnBuscarDiaSemana";
            this.btnBuscarDiaSemana.Size = new System.Drawing.Size(103, 34);
            this.btnBuscarDiaSemana.TabIndex = 57;
            this.btnBuscarDiaSemana.Text = "Buscar";
            this.btnBuscarDiaSemana.UseVisualStyleBackColor = true;
            this.btnBuscarDiaSemana.Click += new System.EventHandler(this.btnBuscarDiaSemana_Click);
            // 
            // btnBuscarDescripcion
            // 
            this.btnBuscarDescripcion.Location = new System.Drawing.Point(239, 21);
            this.btnBuscarDescripcion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnBuscarDescripcion.Name = "btnBuscarDescripcion";
            this.btnBuscarDescripcion.Size = new System.Drawing.Size(103, 34);
            this.btnBuscarDescripcion.TabIndex = 56;
            this.btnBuscarDescripcion.Text = "Buscar";
            this.btnBuscarDescripcion.UseVisualStyleBackColor = true;
            this.btnBuscarDescripcion.Click += new System.EventHandler(this.btnBuscarDescripcion_Click);
            // 
            // btnActualizacion
            // 
            this.btnActualizacion.Location = new System.Drawing.Point(15, 370);
            this.btnActualizacion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnActualizacion.Name = "btnActualizacion";
            this.btnActualizacion.Size = new System.Drawing.Size(209, 53);
            this.btnActualizacion.TabIndex = 55;
            this.btnActualizacion.Text = "Actualizar";
            this.btnActualizacion.UseVisualStyleBackColor = true;
            this.btnActualizacion.Click += new System.EventHandler(this.btnActualizacion_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(3, 91);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(791, 272);
            this.dataGridView1.TabIndex = 54;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // txtDiaSemana
            // 
            this.txtDiaSemana.Location = new System.Drawing.Point(513, 30);
            this.txtDiaSemana.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDiaSemana.Name = "txtDiaSemana";
            this.txtDiaSemana.Size = new System.Drawing.Size(132, 22);
            this.txtDiaSemana.TabIndex = 53;
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Location = new System.Drawing.Point(92, 27);
            this.txtDescripcion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(132, 22);
            this.txtDescripcion.TabIndex = 52;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(380, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 17);
            this.label2.TabIndex = 51;
            this.label2.Text = "Dia de la semana:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 17);
            this.label1.TabIndex = 50;
            this.label1.Text = "Descripcion:";
            // 
            // btnEliminarT
            // 
            this.btnEliminarT.Location = new System.Drawing.Point(611, 369);
            this.btnEliminarT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnEliminarT.Name = "btnEliminarT";
            this.btnEliminarT.Size = new System.Drawing.Size(143, 53);
            this.btnEliminarT.TabIndex = 62;
            this.btnEliminarT.Text = "Eliminar";
            this.btnEliminarT.UseVisualStyleBackColor = true;
            this.btnEliminarT.Click += new System.EventHandler(this.btnEliminarT_Click);
            // 
            // FormBuscarTurno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(806, 443);
            this.Controls.Add(this.btnEliminarT);
            this.Controls.Add(this.BtnModificarTurno);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnBuscarDiaSemana);
            this.Controls.Add(this.btnBuscarDescripcion);
            this.Controls.Add(this.btnActualizacion);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.txtDiaSemana);
            this.Controls.Add(this.txtDescripcion);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FormBuscarTurno";
            this.Text = "FormBuscarTurno";
            this.Load += new System.EventHandler(this.FormBuscarTurno_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnModificarTurno;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnBuscarDiaSemana;
        private System.Windows.Forms.Button btnBuscarDescripcion;
        private System.Windows.Forms.Button btnActualizacion;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox txtDiaSemana;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnEliminarT;
    }
}