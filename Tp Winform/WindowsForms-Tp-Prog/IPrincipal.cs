﻿using Logica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WindowsForms_Tp_Prog
{
    interface IPrincipal
    {
        //Buscar Emleado
        Empleado BuscarEmpleadoPorLegajo(int? legajo);
        object BuscarParcial(string text);
        void EliminarEmpleado(int legajo);
        List<Empleado> ObtenerEmpleados(int? legajo);

        // BUcar Turno
        List<Turno> ObtenerTurnos(int? numero);
        void EliminarTurno(string text);
        Turno BuscarTurnoXDescripcion(string text);
        Turno BuscarTurnoXFecha(int fecha);

        // Agg emp
        void CargarEmpleado(string nombre, string domicilio, string localidad, decimal sueldo, int telefono, string correo, int estado, string turno, int dia, DateTime horadellegada, DateTime horadesalida, int estado2);
        Turno CargarTurnosEmpleado(Empleado empleado, string turno, int dia, DateTime horadellegada, DateTime horadesalida, int estado2);


        //Agg Turno
        void GuardarTurno();
        void CargarTurnos(string turno, int dia, DateTime horadellegada, DateTime horadesalida, int estado2);
        //Turno CargarTurnosEmpleado(Empleado empleado, string turno, int dia, DateTime horadellegada, DateTime horadesalida, int estado2);

        //Mod emp
        void ModificarEmpleado(int legajo, string nombre, string domicilio, string correo, string localidad, int telefono, int estado, decimal sueldoneto);
        void GuardarEmpleado();
        //ingreo
        List<Asistencia> RegistrarEntrada(DateTime ingreso, int legajo, DateTime salida, DateTime fecha, string dia, string turno);
        void GuardarRegistro(int legajo, string dia, DateTime ingreso, DateTime salida, DateTime fecha, string turno);
        List<Asistencia> ObtenerRegistro(int? legajo);
    }
}
