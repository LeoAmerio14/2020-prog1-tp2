﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logica;

namespace WindowsForms_Tp_Prog
{
    interface IBuscarTurno
    {
        List<Turno> ObtenerTurnos(int numero);
        void EliminarTurno(string text);
        Turno BuscarTurnoXDescripcion(string text);
        Turno BuscarTurnoXFecha(int fecha);
    }
}
