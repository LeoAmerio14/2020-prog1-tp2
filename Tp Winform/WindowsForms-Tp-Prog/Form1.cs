﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms_Tp_Prog
{
    public partial class Form1 : Form
    {
        Principal empleado = new Principal();
        public Form1()
        {
            InitializeComponent();
        }
        private void txtLegajo_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            empleado.LeerArchivoEmpleado();
            dataGridView1.DataSource = empleado.BuscarEmpleadoPorLegajo(Convert.ToInt32(txtLegajo.Text));
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnBuscarNombre_Click(object sender, EventArgs e)
        {
            if (txtNombre != null)
            {     
                // condicion q me hace buscar completo
                    empleado.LeerArchivoEmpleado();
                    dataGridView1.DataSource = empleado.BuscarEmpleadoPorNombre(txtNombre.Text);
                //else
                    empleado.LeerArchivoEmpleado();
                    dataGridView1.DataSource = empleado.BuscarParcial(txtNombre.Text);
            }
        }
        
        private void btnActualizacion_Click(object sender, EventArgs e)
        {
            Actualizar();
        }
        private void Actualizar()
        {
            List<Empleado> empleados = empleado.ObtenerEmpleados(null);

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = empleados;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            List<Empleado> empleados = empleado.ObtenerEmpleados(null);

            dataGridView1.DataSource = empleados;
            dataGridView1.AutoGenerateColumns = false;
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            empleado.LeerArchivoEmpleado();
            //dataGridView1.DataSource = empleado.ModificarEmpleado();//debe pasar un Empleado empleado y un numero
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Remove(dataGridView1.CurrentRow);
            empleado.EliminarEmpleado(Convert.ToInt32(txtLegajo.Text));
        }
    }
}
