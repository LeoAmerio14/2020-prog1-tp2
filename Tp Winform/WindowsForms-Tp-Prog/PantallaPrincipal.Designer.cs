﻿namespace WindowsForms_Tp_Prog
{
    partial class PantallaPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnRegistrarIngreso = new System.Windows.Forms.Button();
            this.BtnAgregarTurno = new System.Windows.Forms.Button();
            this.BtnAgregarEmpleado = new System.Windows.Forms.Button();
            this.BtnEmpleadoPrincipal = new System.Windows.Forms.Button();
            this.btnBuscarTurno = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BtnRegistrarIngreso
            // 
            this.BtnRegistrarIngreso.Location = new System.Drawing.Point(131, 217);
            this.BtnRegistrarIngreso.Margin = new System.Windows.Forms.Padding(4);
            this.BtnRegistrarIngreso.Name = "BtnRegistrarIngreso";
            this.BtnRegistrarIngreso.Size = new System.Drawing.Size(151, 38);
            this.BtnRegistrarIngreso.TabIndex = 10;
            this.BtnRegistrarIngreso.Text = "Ingreso/Salida";
            this.BtnRegistrarIngreso.UseVisualStyleBackColor = true;
            this.BtnRegistrarIngreso.Click += new System.EventHandler(this.BtnRegistrarIngreso_Click);
            // 
            // BtnAgregarTurno
            // 
            this.BtnAgregarTurno.Location = new System.Drawing.Point(248, 114);
            this.BtnAgregarTurno.Margin = new System.Windows.Forms.Padding(4);
            this.BtnAgregarTurno.Name = "BtnAgregarTurno";
            this.BtnAgregarTurno.Size = new System.Drawing.Size(151, 38);
            this.BtnAgregarTurno.TabIndex = 9;
            this.BtnAgregarTurno.Text = "Agregar turno";
            this.BtnAgregarTurno.UseVisualStyleBackColor = true;
            this.BtnAgregarTurno.Click += new System.EventHandler(this.button1_Click);
            // 
            // BtnAgregarEmpleado
            // 
            this.BtnAgregarEmpleado.Location = new System.Drawing.Point(13, 114);
            this.BtnAgregarEmpleado.Margin = new System.Windows.Forms.Padding(4);
            this.BtnAgregarEmpleado.Name = "BtnAgregarEmpleado";
            this.BtnAgregarEmpleado.Size = new System.Drawing.Size(151, 38);
            this.BtnAgregarEmpleado.TabIndex = 7;
            this.BtnAgregarEmpleado.Text = "Agregar empleado";
            this.BtnAgregarEmpleado.UseVisualStyleBackColor = true;
            this.BtnAgregarEmpleado.Click += new System.EventHandler(this.BtnAgregarEmpleado_Click);
            // 
            // BtnEmpleadoPrincipal
            // 
            this.BtnEmpleadoPrincipal.Location = new System.Drawing.Point(13, 68);
            this.BtnEmpleadoPrincipal.Margin = new System.Windows.Forms.Padding(4);
            this.BtnEmpleadoPrincipal.Name = "BtnEmpleadoPrincipal";
            this.BtnEmpleadoPrincipal.Size = new System.Drawing.Size(151, 38);
            this.BtnEmpleadoPrincipal.TabIndex = 11;
            this.BtnEmpleadoPrincipal.Text = "Buscar Empleado";
            this.BtnEmpleadoPrincipal.UseVisualStyleBackColor = true;
            this.BtnEmpleadoPrincipal.Click += new System.EventHandler(this.BtnEmpleadoPrincipal_Click);
            // 
            // btnBuscarTurno
            // 
            this.btnBuscarTurno.Location = new System.Drawing.Point(248, 68);
            this.btnBuscarTurno.Margin = new System.Windows.Forms.Padding(4);
            this.btnBuscarTurno.Name = "btnBuscarTurno";
            this.btnBuscarTurno.Size = new System.Drawing.Size(151, 38);
            this.btnBuscarTurno.TabIndex = 12;
            this.btnBuscarTurno.Text = "Buscar Turno";
            this.btnBuscarTurno.UseVisualStyleBackColor = true;
            this.btnBuscarTurno.Click += new System.EventHandler(this.btnBuscarTurno_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Tai Le", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 31);
            this.label1.TabIndex = 13;
            this.label1.Text = "Empleados:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Tai Le", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(143, 167);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 31);
            this.label2.TabIndex = 14;
            this.label2.Text = "Registros:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Tai Le", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(243, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 31);
            this.label4.TabIndex = 16;
            this.label4.Text = "Turnos:";
            // 
            // PantallaPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(453, 447);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnBuscarTurno);
            this.Controls.Add(this.BtnEmpleadoPrincipal);
            this.Controls.Add(this.BtnRegistrarIngreso);
            this.Controls.Add(this.BtnAgregarTurno);
            this.Controls.Add(this.BtnAgregarEmpleado);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "PantallaPrincipal";
            this.Text = "PantallaPrincipal";
            this.Load += new System.EventHandler(this.PantallaPrincipal_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnRegistrarIngreso;
        private System.Windows.Forms.Button BtnAgregarTurno;
        private System.Windows.Forms.Button BtnAgregarEmpleado;
        private System.Windows.Forms.Button BtnEmpleadoPrincipal;
        private System.Windows.Forms.Button btnBuscarTurno;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
    }
}