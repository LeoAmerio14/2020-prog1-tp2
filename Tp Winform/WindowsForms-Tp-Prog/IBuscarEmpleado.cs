﻿using Logica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsForms_Tp_Prog
{
    interface IBuscarEmpleado
    {
        Empleado BuscarEmpleadoPorLegajo(int legajo);
        object BuscarParcial(string text);
        void EliminarEmpleado(int legajo);
        List<Empleado> ObtenerEmpleados(int? legajo);
    }
}
