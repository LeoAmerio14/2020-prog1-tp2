﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logica;

namespace WindowsForms_Tp_Prog
{
    public partial class FormBuscarTurno : Form, IBuscarTurno
    {
        public FormBuscarTurno()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            IPrincipal principal = this.Owner as IPrincipal;
            List<Turno> turnos = principal.ObtenerTurnos(null);

            dataGridView1.DataSource = turnos;
            dataGridView1.AutoGenerateColumns = false;
        }

        private void btnActualizacion_Click(object sender, EventArgs e)
        {
            IPrincipal principal = this.Owner as IPrincipal;
            List<Turno> turnos = principal.ObtenerTurnos(null);

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = turnos;
        }

        private void btnEliminarT_Click(object sender, EventArgs e)
        {
            IPrincipal principal = this.Owner as IPrincipal;
            principal.EliminarTurno(txtDescripcion.Text);
        }

        private void btnBuscarDescripcion_Click(object sender, EventArgs e)
        {
            IPrincipal principal = this.Owner as IPrincipal;
            Turno turno = principal.BuscarTurnoXDescripcion(txtDescripcion.Text);
            var buscar = new List<Turno>();
            buscar.Add(turno);
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = buscar;
        }

        private void btnBuscarDiaSemana_Click(object sender, EventArgs e)
        {
            IPrincipal principal = this.Owner as IPrincipal;
            Turno turno = principal.BuscarTurnoXFecha(Convert.ToInt32(txtDiaSemana.Text));
            var buscarDia = new List<Turno>();
            buscarDia.Add(turno);
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = buscarDia;
        }

        private void FormBuscarTurno_Load(object sender, EventArgs e)
        {
            IPrincipal principal = this.Owner as IPrincipal;
            List<Turno> turnos = principal.ObtenerTurnos(null);

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = turnos;
        }

        public List<Turno> ObtenerTurnos(int numero)
        {
            IPrincipal principal = this.Owner as IPrincipal;
            List<Turno> turno = new List<Turno>();
            return turno = principal.ObtenerTurnos(numero);
        }

        public void EliminarTurno(string text)
        {
            IPrincipal principal = this.Owner as IPrincipal;
            principal.EliminarTurno(text);
        }

        public Turno BuscarTurnoXDescripcion(string text)
        {
            IPrincipal principal = this.Owner as IPrincipal;
            Turno turno = new Turno();
            return turno = principal.BuscarTurnoXDescripcion(text);
        }

        public Turno BuscarTurnoXFecha(int fecha)
        {
            IPrincipal principal = this.Owner as IPrincipal;
            Turno turno = new Turno();
            return turno = principal.BuscarTurnoXFecha(fecha);
        }
    }
}
