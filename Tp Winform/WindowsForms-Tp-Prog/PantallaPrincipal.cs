﻿using Logica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms_Tp_Prog
{
    public partial class PantallaPrincipal : Form, IPrincipal
    {
        public Principal servicioPrincial { get; set; }
        
        public PantallaPrincipal()
        {
            InitializeComponent();
            servicioPrincial = new Principal();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Form nuevo = new FormAgregarTurno();
            nuevo.Owner = this;
            nuevo.Show();
        }

        private void BtnAgregarEmpleado_Click(object sender, EventArgs e)
        {
            Form nuevo = new FormAgregarEmpleado();
            nuevo.Owner = this;
            nuevo.Show();
        }

        private void BtnEmpleadoPrincipal_Click(object sender, EventArgs e)
        {
            Form nuevo = new FormBuscarEmpleado();
            nuevo.Owner = this; 
            nuevo.Show();
        }

        private void btnBuscarTurno_Click(object sender, EventArgs e)
        {
            Form nuevo = new FormBuscarTurno();
            nuevo.Owner = this;
            nuevo.Show();
        }

        private void BtnModificarTurno_Click(object sender, EventArgs e)
        {
            Form nuevo = new FormAgregarEmpleado();
            nuevo.Owner = this;
            nuevo.Show();
        }

        private void BtnRegistrarIngreso_Click(object sender, EventArgs e)
        {
            Form nuevo = new Ingreso();
            nuevo.Owner = this;
            nuevo.Show();
        }

       

        public void EliminarEmpleado(int legajo)
        {
            servicioPrincial.EliminarEmpleado(legajo);
        }

        public void CargarEmpleado(string nombre, string domicilio, string localidad, decimal sueldo, int telefono, string correo, int estado, string turno, int dia, DateTime horadellegada, DateTime horadesalida, int estado2)
        {
            servicioPrincial.CargarEmpleado(nombre, domicilio, localidad, sueldo, telefono, correo, estado, turno, dia, horadellegada, horadesalida, estado2);
        }

        Empleado IPrincipal.BuscarEmpleadoPorLegajo(int? legajo)
        {
            return servicioPrincial.BuscarEmpleadoPorLegajo(legajo);
        }

        object IPrincipal.BuscarParcial(string text)
        {
            return servicioPrincial.BuscarParcial(text);
        }

        List<Empleado> IPrincipal.ObtenerEmpleados(int? legajo)
        {
            return servicioPrincial.ObtenerEmpleados(legajo);
        }

        public void ModificarEmpleado(int legajo, string nombre, string domicilio, string correo, string localidad, int telefono, int estado, decimal sueldoneto)
        {
            servicioPrincial.ModificarEmpleado(legajo, nombre, domicilio, correo, localidad, telefono, estado, sueldoneto);
        }

        public void GuardarEmpleado()
        {
            servicioPrincial.GuardarEmpleado();
        }

        private void PantallaPrincipal_Load(object sender, EventArgs e)
        {

        }

        public List<Turno> ObtenerTurnos(int? numero)
        {
            return servicioPrincial.ObtenerTurnos(numero);
        }

        public void EliminarTurno(string text)
        {
            servicioPrincial.EliminarTurno(text);
        }

        public Turno BuscarTurnoXDescripcion(string text)
        {
            return servicioPrincial.BuscarTurnoXDescripcion(text);
        }

        public Turno BuscarTurnoXFecha(int fecha)
        {
            return servicioPrincial.BuscarTurnoXFecha(fecha);
        }

        public Turno CargarTurnosEmpleado(Empleado empleado, string turno, int dia, DateTime horadellegada, DateTime horadesalida, int estado2)
        {
            return servicioPrincial.CargarTurnosEmpleado(empleado, turno, dia, horadellegada, horadesalida, estado2);
        }
        
        public void CargarTurnos(string turno, int dia, DateTime horadellegada, DateTime horadesalida, int estado2)
        {
            servicioPrincial.CargarTurnos(turno, dia, horadellegada, horadesalida, estado2);
        }

        public void GuardarTurno()
        {
            servicioPrincial.GuardarTurno();
        }

        public List<Asistencia> RegistrarEntrada(DateTime ingreso, int legajo, DateTime salida, DateTime fecha, string dia, string turno)
        {
            List<Asistencia> asistencias = new List<Asistencia>();
            return asistencias = servicioPrincial.RegistrarEntrada(ingreso, legajo, salida, fecha, dia, turno);
        }

        public void GuardarRegistro(int legajo, string dia, DateTime ingreso, DateTime salida, DateTime fecha, string turno)
        {
            servicioPrincial.GuardarRegistro(legajo, dia, ingreso, salida, fecha, turno);
        }

        public List<Asistencia> ObtenerRegistro(int? legajo)
        {
            List<Asistencia> asistencias = new List<Asistencia>();
            return asistencias = servicioPrincial.ObtenerRegistro(legajo);
        }
    }
}
